#include <stdio.h>
#include <unistd.h>


int main(
        int argc,
        char ** argv) {
    char * fileName = argc > 1 ? argv[1] : "file.txt";

    int uid = getuid();
    int euid = geteuid();

    int errcode;

    printf("Real UID\t= %d\n", uid);
    printf("Effective UID\t= %d\n", euid);

    FILE * file = fopen(fileName, "rw");
    if (file == NULL) {
        perror("Failed to open file");
        return -1;
    }
    printf("Successfully opened file\n");

    errcode = fclose(file);
    if (errcode != 0) {
        perror("Failed to close the file");
        return -1;
    }
    printf("Successfully closed file\n");

    errcode = setuid(uid);
    if (errcode != 0) {
        perror("setuid() failed");
        return -1;
    }

    printf("Real UID\t= %d\n", getuid());
    printf("Effective UID\t= %d\n", geteuid());

    file = fopen(fileName, "rw");
    if (file == NULL) {
        perror("Failed to open file");
        return -1;
    }
    printf("Successfully opened file\n");

    errcode = fclose(file);
    if (errcode != 0) {
        perror("Failed to close the file");
        return -1;
    }
    printf("Successfully closed file\n");

    return 0;
}
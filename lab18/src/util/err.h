#ifndef LAB18_ERR_H
#define LAB18_ERR_H

#define OK                      0
#define FAIL                    (-10)
#define SYSTEM_CALL_ERROR       (-1)
#define INVALID_ARGUMENT_ERROR  (-2)

#define ERROR_POINTER           (NULL)

#endif //LAB18_ERR_H

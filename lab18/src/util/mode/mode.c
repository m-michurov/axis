#include <sys/stat.h>
#include <stdlib.h>

#include "mode.h"

#define CHECK(mode, mask)      ((mode) & (mask))

int fillModeString(
        int mode,
        mode_string_t modeString) {
    if (modeString == NULL) {
        return INVALID_ARGUMENT_ERROR;
    }
    modeString[MODE_TYPE] =
            S_ISDIR(mode)
            ? DIRECTORY
            : S_ISREG(mode)
              ? REGULAR_FILE
              : UNKNOWN;

    modeString[USER_READ] = CHECK(mode, S_IRUSR) ? READ_PERMISSION : NO_PERMISSION;
    modeString[USER_WRITE] = CHECK(mode, S_IWUSR) ? WRITE_PERMISSION : NO_PERMISSION;
    modeString[USER_EXECUTE] = CHECK(mode, S_IXUSR) ? EXECUTE_PERMISSION : NO_PERMISSION;

    modeString[GROUP_READ] = CHECK(mode, S_IRGRP) ? READ_PERMISSION : NO_PERMISSION;
    modeString[GROUP_WRITE] = CHECK(mode, S_IWGRP) ? WRITE_PERMISSION : NO_PERMISSION;
    modeString[GROUP_EXECUTE] = CHECK(mode, S_IXGRP) ? EXECUTE_PERMISSION : NO_PERMISSION;

    modeString[OTHERS_READ] = CHECK(mode, S_IROTH) ? READ_PERMISSION : NO_PERMISSION;
    modeString[OTHERS_WRITE] = CHECK(mode, S_IWOTH) ? WRITE_PERMISSION : NO_PERMISSION;
    modeString[OTHERS_EXECUTE] = CHECK(mode, S_IXOTH) ? EXECUTE_PERMISSION : NO_PERMISSION;
}

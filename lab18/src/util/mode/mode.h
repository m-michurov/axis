#ifndef LAB18_MODE_H
#define LAB18_MODE_H

#include "../err.h"

typedef char mode_string_t[11];

#define EMPTY_MODE_STRING   ("----------")

#define MODE_TYPE           (0)
#define USER_READ      (1)
#define USER_WRITE     (2)
#define USER_EXECUTE   (3)
#define GROUP_READ     (4)
#define GROUP_WRITE    (5)
#define GROUP_EXECUTE  (6)
#define OTHERS_READ    (7)
#define OTHERS_WRITE   (8)
#define OTHERS_EXECUTE (9)

#define REGULAR_FILE        ('-')
#define DIRECTORY           ('d')
#define UNKNOWN             ('?')

#define NO_PERMISSION       ('-')
#define READ_PERMISSION     ('r')
#define WRITE_PERMISSION    ('w')
#define EXECUTE_PERMISSION  ('x')

/*
 * Fill file type and permissions in a mode_string_t object
 * pointed by `modeString` based on bit mask `mode` acquired
 * from sruct stat.
 */
int fillModeString(
        int mode,
        mode_string_t modeString);

#endif //LAB18_MODE_H

#include <stdio.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>
#include <time.h>
#include <libgen.h>
#include <errno.h>

#include "util/mode/mode.h"

#define MIN_ARGC    (2)

/*
 * Print file info in the following format:
 *
 * `<mode>` `<links>` `<owner>` `<group>` `<file size>` `<last modified>` `<file name>`
 *
 * Where:
 *     `mode` is 10 characters string representing file type and access rights:
 *         First character is either 'd' for directory, '-' for a regular file or '?' for any other file type
 *         Next three groups of three characters each represent read ('r'), write ('w') and execute ('x')
 *         permissions for user, group and other. If permission is not set, then corresponding character is '-'.
 *
 *     `links` is the links count of the file.
 *     `owner` is the name of file's owner, if accessible, and owner's user id otherwise.
 *     `group` is the name of file's group, if accessible, and file's group id otherwise.
 *
 *     `owner` and `group` are truncated to 12 characters.
 *
 *     'file size' is the size of the file in bytes for regular files and is empty for other file types.
 *     `last modified` is a string representing the time when the file was last modified.
 *     `file name` is the name passed to function without path prefix.
 *
 * Please note that a string pointed by `name` may be modified in process. Consider passing a copy.
 */
void printFileInfo(
        char * name,
        struct stat * stat) {
    mode_string_t modeString = EMPTY_MODE_STRING;

    fillModeString(stat->st_mode, modeString);

    int uid = stat->st_uid;
    struct passwd * passwdEntry = getpwuid(uid);

    int gid = stat->st_gid;
    struct group * groupEntry = getgrgid(gid);

    char * lastModified = ctime(&stat->st_ctime);
    char * fileName = basename(name);

    printf("%s %12lu ", modeString, stat->st_nlink);
    if (passwdEntry != ERROR_POINTER) {
        printf("%12.12s ", passwdEntry->pw_name);
    } else {
        printf("%12d ", uid);
    }

    if (groupEntry != ERROR_POINTER) {
        printf("%12.12s ", groupEntry->gr_name);
    } else {
        printf("%12d ", gid);
    }

    if (modeString[MODE_TYPE] == REGULAR_FILE) {
        printf("%12lu  ", stat->st_size);
    } else {
        printf("%12s  ", "");
    }

    printf("%.24s  %s\n", lastModified, fileName);
}


int main(
        int argc,
        char ** argv) {
    char * defaultArgs[] = { ".", NULL };

    char ** entries = argc >= MIN_ARGC ? argv + 1 : defaultArgs;

    struct stat stat;
    char * currentEntry;
    int err;

    for (char ** pCurrentEntry = entries; *pCurrentEntry != NULL; pCurrentEntry += 1) {
        currentEntry = *pCurrentEntry;
        err = lstat(currentEntry, &stat);

        if (err == SYSTEM_CALL_ERROR) {
            fprintf(stderr, "Failed to obtain file data for \"%s\": %s\n", currentEntry, strerror(errno));
            continue;
        }

        printFileInfo(currentEntry, &stat);
    }

    return OK;
}

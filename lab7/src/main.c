#include <stdio.h>
#include <string.h>


#include "utility/line_data_table.h"
#include "error_reporter/error.h"
#include "utility/utility.h"


#define BUFFER_SIZE BUFSIZ
#define forever_loop while(1)


int prepareData(
        int argc,
        char ** argv,
        char const ** pErrorDescription,
        char ** pFilePointer,
        int * pFileLength,
        LineDataTable * pDataTable);


int runUserInteraction(
        char const ** pErrorDescription,
        char * filePointer,
        int fileLength,
        LineDataTable * pDataTable);


int cleanup(
        char ** pFilePointer,
        int fileLength,
        LineDataTable * pLineData);


int main(
        int argc,
        char ** argv) {
    char const * errorDescription = NULL;
    int errcode;

    char * filePointer;
    int fileLength;

    LineDataTable lineDataTable = { NULL, 0, 0 };

    errcode = prepareData(argc, argv, &errorDescription, &filePointer, &fileLength, &lineDataTable);
    if (errcode != OK) {
        reportError(errorDescription, errcode);
        return FAIL;
    }

    errcode = runUserInteraction(&errorDescription, filePointer, fileLength, &lineDataTable);
    reportError(errorDescription, errcode);

    return cleanup(&filePointer, fileLength, &lineDataTable);
}


int prepareData(
        int argc,
        char ** argv,
        char const ** pErrorDescription,
        char ** pFilePointer,
        int * pFileLength,
        LineDataTable * pDataTable) {
    if (pErrorDescription == NULL || argv == NULL || pFilePointer == NULL
            || pFileLength == NULL || pDataTable == NULL) {
        fprintf(stderr, "Failed to prepare necessary data: invalid program state unrelated to user input.\n");
        return NULL_POINTER_ERROR;
    }

    if (argc < 2) {
        *pErrorDescription = "No input file specified";
        return FAIL;
    }

    char * inputFilePath = argv[1];

    int errcode = mapFile(inputFilePath, pFilePointer, pFileLength);
    if (errcode != OK) {
        *pErrorDescription = "Failed to prepare necessary data: failed to map file";
        return errcode;
    }

    errcode = fillOffsetTable(pErrorDescription, *pFilePointer, *pFileLength, pDataTable);
    if (errcode != OK) {
        *pErrorDescription = "Failed to prepare necessary data: failed to fill offset table";
        return errcode;
    }

    return OK;
}


int runUserInteraction(
        char const ** pErrorDescription,
        char * filePointer,
        int fileLength,
        LineDataTable * pDataTable) {
    if (pErrorDescription == NULL || pDataTable == NULL) {
        fprintf(stderr, "Failed to run user interaction: invalid program state unrelated to user input.\n");
        return NULL_POINTER_ERROR;
    }

    int errcode;
    char buffer[BUFFER_SIZE] = { 0 };
    int lineIndex;
    int length;
    int offset;
    int maxAllowedIndex = getLastIndex(pDataTable);

    printf("Enter line number from allowed range [1, %d] or 0 to exit.\n", maxAllowedIndex);
    forever_loop {
        printf("Enter line number within 5 seconds:\n");
        errcode = getLineNumber(&lineIndex, pErrorDescription, buffer, BUFFER_SIZE);

        if (errcode == READ_TIMED_OUT) {
            printf("No input within 5 seconds. Printing the entire file:\n");
            return printEntireFile(filePointer, fileLength);
        }
        if (errcode != OK && errcode != BAD_INPUT_RANGE) {
            return errcode;
        }
        if (lineIndex < 0 || lineIndex > maxAllowedIndex || errcode == BAD_INPUT_RANGE) {
            fprintf(stderr, "Allowed range is [1, %d].\n", maxAllowedIndex);
            *pErrorDescription = "Line number out of allowed range";
            return BAD_INPUT_RANGE;
        }
        if (lineIndex == 0) {
            break;
        }
        length = getLength(pDataTable, lineIndex);
        offset = getOffset(pDataTable, lineIndex);
        errcode = printLineFromFile(filePointer, length, offset);
        if (errcode != OK) {
            *pErrorDescription = "Failed to reprint line";
            return errcode;
        }
    }

    return OK;
}


int cleanup(
        char ** pFilePointer,
        int fileLength,
        LineDataTable * pLineData) {
    clearOffsetTable(pLineData);
    int errcode = unmapFile(*pFilePointer, fileLength);
    *pFilePointer = NULL;

    return errcode;
}

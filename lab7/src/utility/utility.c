#include "utility.h"


#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <poll.h>
#include <sys/mman.h>


#define TOTAL_FDS       1
#define DEFAULT_TIMEOUT 5000
#define POLL_TIMED_OUT  0


int fillOffsetTable(
        const char ** pErrorDescription,
        const char * filePointer,
        int fileLength,
        LineDataTable * dataTable) {
    if (pErrorDescription == NULL || dataTable == NULL || filePointer == NULL) {
        fprintf(stderr, "Failed to fill offset table: invalid program state unrelated to user input.\n");
        return NULL_POINTER_ERROR;
    }
    int currentLineLength = 0;
    int lineIndex = 1;
    int errcode;

    errcode = setOffset(dataTable, 1, 0);
    if (errcode != OK) {
        *pErrorDescription = "Failed to fill offset table: failed to set line offset due to memory allocation error";
        return errcode;
    }

    for (int offset = 0; ; offset += 1) {
        currentLineLength = 0;

        while (offset < fileLength && filePointer[offset] != '\n') {
            currentLineLength += 1;
            offset += 1;
        }

        if (offset == fileLength) {
            break;
        }

        errcode = setLength(dataTable, lineIndex, currentLineLength);
        if (errcode != OK) {
            *pErrorDescription = "Failed to fill offset table: failed to set line length due to memory allocation error";
            return errcode;
        }
        errcode = setOffset(dataTable, lineIndex + 1, offset + 1);
        if (errcode != OK) {
            *pErrorDescription = "Failed to fill offset table: failed to set line offset due to memory allocation error";
            return errcode;
        }
        lineIndex += 1;
    }

    errcode = setLength(dataTable, lineIndex, currentLineLength);
    if (errcode != OK) {
        *pErrorDescription = "Failed to fill offset table: failed to set line length";
    }

    return errcode;
}


int printLineFromFile(
        char * filePointer,
        int length,
        int offset) {
    if (filePointer == NULL) {
        fprintf(stderr, "Filed to print line:  invalid program state unrelated to user input.\n");
        return NULL_POINTER_ERROR;
    }

    fwrite(filePointer + offset, sizeof(char), length, stdout);

    printf("\n");

    return OK;
}


int printEntireFile(
        char * filePointer,
        int fileLength) {
    if (filePointer == NULL) {
        fprintf(stderr, "Failed to print entire file: invalid program state unrelated to user input.\n");
        return NULL_POINTER_ERROR;
    }

    fwrite(filePointer, sizeof(char), fileLength, stdout);

    printf("\n");

    return OK;
}


int closeAndUnmapFile(
        int fileDesc,
        char * filePointer,
        int fileLength) {
    int errcode = OK;

    if (fileDesc != SYSTEM_CALL_ERROR) {
        errcode = close(fileDesc);
        if (errcode == SYSTEM_CALL_ERROR) {
            perror("Failed to close input file");
        }
    }

    int auxErrcode = unmapFile(filePointer, fileLength);

    return errcode == OK && auxErrcode == OK ? OK : SYSTEM_CALL_ERROR;
}


int mapFile(
        char * filePath,
        char ** pFilePointer,
        int * pFileLength) {
    if (filePath == NULL || pFilePointer == NULL || pFileLength == NULL) {
        fprintf(stderr, "Invalid program state unrelated to user input\n");
        return NULL_POINTER_ERROR;
    }
    int fileDesc;
    char * filePointer = MAP_FAILED;
    int length;

    fileDesc = open(filePath, O_RDONLY);
    if (fileDesc == SYSTEM_CALL_ERROR) {
        fprintf(stderr, "Failed to open \"%s\": %s\n", filePath, strerror(errno));
        return SYSTEM_CALL_ERROR;
    }

    length = lseek(fileDesc, 0, SEEK_END);
    if (length == SYSTEM_CALL_ERROR) {
        perror("Failed to determine input file length");
        closeAndUnmapFile(fileDesc, filePointer, length);
        return SYSTEM_CALL_ERROR;
    }

    filePointer = (char *) mmap(NULL, length > 0 ? length : 1, PROT_READ, MAP_PRIVATE, fileDesc, (off_t) 0);
    if (filePointer == MAP_FAILED) {
        perror("Failed to map input file to virtual memory");
        closeAndUnmapFile(fileDesc, filePointer, length);
        return SYSTEM_CALL_ERROR;
    }

    int errcode = close(fileDesc);
    if (errcode == SYSTEM_CALL_ERROR) {
        perror("Failed to close input file after it was mapped to memory");
        return SYSTEM_CALL_ERROR;
    }

    *pFilePointer = filePointer;
    *pFileLength = length;

    return OK;
}


int unmapFile(
        char * filePointer,
        int fileLength) {
    if (filePointer == MAP_FAILED || fileLength < 0) {
        return FAIL;
    }

    int errcode = munmap(filePointer, fileLength > 0 ? fileLength : 1);
    if (errcode == SYSTEM_CALL_ERROR) {
        perror("Failed to unmap input file");
        return SYSTEM_CALL_ERROR;
    }

    return OK;
}


int parseInt(
        char * buffer,
        unsigned int length,
        int * resultInt) {
    if (resultInt == NULL) {
        return NULL_POINTER_ERROR;
    }

    char * nextCharPtr;
    long result = strtol(buffer, &nextCharPtr, 10);

    if (nextCharPtr == buffer || errno == EINVAL) {
        return INPUT_IS_NON_INTEGER;
    }

    for (char * it = nextCharPtr; it != buffer + length; ++it) {
        if (!isspace(*it) && *it != '\n') {
            return INPUT_IS_NON_INTEGER;
        }
    }

    if (errno == ERANGE || result > INT_MAX || result < INT_MIN) {
        return BAD_INPUT_RANGE;
    }

    *resultInt = (int) result;

    return OK;
}


int readWithTimeout(
        char const ** pErrorDescription,
        char * buffer,
        int bufferSize,
        int timeout) {
    if (pErrorDescription == NULL || buffer == NULL) {
        fprintf(stderr, "Failed to read input with timeout: invalid program state unrelated to user input.\n");
        return NULL_POINTER_ERROR;
    }
    if (bufferSize < 4) {
        fprintf(stderr, "Failed to read input with timeout: invalid program state unrelated to user input.\n");
        return BAD_ARGUMENT_RANGE;
    }
    struct pollfd fd;
    fd.fd = STDIN_FILENO;
    fd.events = POLLIN;

    int retValue = poll(&fd, TOTAL_FDS, timeout);
    if (retValue == SYSTEM_CALL_ERROR) {
        perror("Failed to read input with timeout");
        return SYSTEM_CALL_ERROR;
    }
    if (retValue == POLL_TIMED_OUT) {
        return READ_TIMED_OUT;
    }
    if (fd.revents & POLLERR || fd.revents & POLLNVAL || fd.revents & POLLHUP) { // NOLINT
        fprintf(stderr, "Failed to read input with timeout: unexpected error with standard input stream\n");
        return FAIL;
    }

    char * retPtr = fgets(buffer, bufferSize, stdin);
    if (retPtr != buffer) {
        *pErrorDescription = "Failed to get line number: unable to read input from standard input stream";
        return FAIL;
    }

    return OK;
}


int getLineNumber(
        int * result,
        char const ** pErrorDescription,
        char * buffer,
        int bufferSize) {
    if (result == NULL || pErrorDescription == NULL || buffer == NULL) {
        fprintf(stderr, "Failed to get line number: invalid program state unrelated to user input.\n");
        return NULL_POINTER_ERROR;
    }
    if (bufferSize < 4) {
        fprintf(stderr, "Failed to get line number: invalid program state unrelated to user input.\n");
        return BAD_ARGUMENT_RANGE;
    }

    int errcode;
    int lineNumber;

    errcode = readWithTimeout(pErrorDescription, buffer, bufferSize, DEFAULT_TIMEOUT);
    if (errcode != OK) {
        return errcode;
    }

    unsigned int length = strlen(buffer);
    if (length == bufferSize - 1 && buffer[length - 1] != '\n') {
        fprintf(stderr, "Please limit your input to %d characters\n", bufferSize - 2);
        *pErrorDescription = "Input is too long";
        return BAD_INPUT_FORMAT;
    }

    errcode = parseInt(buffer, length, &lineNumber);
    if (errcode != OK) {
        *pErrorDescription = "Failed to get line number: bad input format";
        return errcode;
    }

    *result = lineNumber;
    return OK;
}

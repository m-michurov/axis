#include "line_data_table.h"
#include "../error_reporter/error.h"


#include <stdlib.h>


struct st_LineData {
    int m_length;
    off_t m_offset;
};


// A utility function used to resize the table if there's not enough space for a new record
int resizeTable(
        LineDataTable * dataTable,
        unsigned int newMaxSize) {
    LineData * tmp = realloc(dataTable->m_data, newMaxSize * sizeof(LineData));

    if (tmp == NULL) {
        return SYSTEM_CALL_ERROR;
    }

    dataTable->m_data = tmp;
    dataTable->m_max_size = (int) newMaxSize;

    return OK;
}

// A utility function used to set data array size, max size and current size to fit a new record for lineIndex
// Uses resizeTable if data array size needs to be increased
int ensureFits(
        LineDataTable * dataTable,
        int lineIndex) {
    if (dataTable == NULL) {
        return NULL_POINTER_ERROR;
    }

    if (lineIndex > dataTable->m_max_size) {
        unsigned int multiplier = 2u;

        if (dataTable->m_max_size < 1) {
            dataTable->m_max_size = 1;
        }

        while (lineIndex > dataTable->m_max_size * multiplier) {
            multiplier <<= 2u;
        }

        int errcode = resizeTable(dataTable, dataTable->m_max_size * multiplier);
        if (errcode != OK) {
            return errcode;
        }
    }

    if (lineIndex > dataTable->m_current_size) {
        dataTable->m_current_size = lineIndex;
    }

    return OK;
}


void clearOffsetTable(LineDataTable * pOffsetTable) {
    if (pOffsetTable == NULL) {
        return;
    }

    free(pOffsetTable->m_data);
    pOffsetTable->m_data = NULL;
    pOffsetTable->m_max_size = 0;
    pOffsetTable->m_current_size = 0;
}


int setLength(
        LineDataTable * dataTable,
        int lineIndex,
        int length) {
    if (dataTable == NULL) {
        return NULL_POINTER_ERROR;
    }
    if (lineIndex < 1 || length < 0) {
        return BAD_ARGUMENT_RANGE;
    }

    // Make sure it's possible to set length for lineIndex
    int errcode = ensureFits(dataTable, lineIndex);
    if (errcode != OK) {
        return errcode;
    }

    dataTable->m_data[lineIndex - 1].m_length = length;

    return OK;
}


int getLength(
        LineDataTable * pOffsetTable,
        int lineIndex) {
    if (pOffsetTable == NULL) {
        return NULL_POINTER_ERROR;
    }
    if (lineIndex < 1 || lineIndex > pOffsetTable->m_current_size) {
        return BAD_ARGUMENT_RANGE;
    }

    return pOffsetTable->m_data[lineIndex - 1].m_length;
}


int setOffset(
        LineDataTable * dataTable,
        int lineIndex,
        off_t offset) {
    if (dataTable == NULL) {
        return NULL_POINTER_ERROR;
    }
    if (lineIndex < 1 || offset < (off_t) 0) {
        return BAD_ARGUMENT_RANGE;
    }

    // Make sure it's possible to set offset for lineIndex
    int errcode = ensureFits(dataTable, lineIndex);
    if (errcode != OK) {
        return errcode;
    }

    dataTable->m_data[lineIndex - 1].m_offset = offset;

    return OK;
}


off_t getOffset(
        LineDataTable * pOffsetTable,
        int lineIndex) {
    if (pOffsetTable == NULL) {
        return NULL_POINTER_ERROR;
    }
    if (lineIndex < 1 || lineIndex > pOffsetTable->m_current_size) {
        return BAD_ARGUMENT_RANGE;
    }

    return pOffsetTable->m_data[lineIndex - 1].m_offset;
}


int getLastIndex(LineDataTable * dataTable) {
    if (dataTable == NULL) {
        return NULL_POINTER_ERROR;
    }

    return dataTable->m_current_size;
}

#ifndef LAB5_UTILITY_H
#define LAB5_UTILITY_H


#include "line_data_table.h"
#include "../error_reporter/error.h"


#define READ_TIMED_OUT  1


int fillOffsetTable(
        const char ** pErrorDescription,
        const char * filePointer,
        int fileLength,
        LineDataTable * dataTable);


int printLineFromFile(
        char * filePointer,
        int length,
        int offset);


int printEntireFile(
        char * filePointer,
        int fileLength);


int getLineNumber(
        int * result,
        char const ** pErrorDescription,
        char * buffer,
        int bufferSize);


int mapFile(
        char * filePath,
        char ** pFilePointer,
        int * pFileLength);


int unmapFile(
        char * filePointer, // File is unmapped only if filePointer is not MAP_FAILED and fileLength is valid
        int fileLength);


#endif //LAB5_UTILITY_H

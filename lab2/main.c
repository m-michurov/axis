#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <errno.h>


extern char *tzname[2]; // contains timezone names


int main() {
    time_t now;
    struct tm * time_info;

    int errcode;

    errcode = setenv("TZ", "America/Los_Angeles", 1);

    if (errcode == -1) {
        fprintf(stderr, "Error changing timezone: %s", strerror(errno));
        return -1;
    }

    errcode = time(&now);

    if (errcode == -1) {
        fprintf(stderr, "Error: time() : %s", strerror(errno));
        return -1;
    }

    time_info = localtime(&now);

    if (time_info == NULL) {
        fprintf(stderr, "Error: localtime() : %s", strerror(errno));
        return -1;
    }

    /*struct tm {
          int tm_sec;         // seconds after minute [0,61]
          int tm_min;         // minutes after the hour [0,59]
          int tm_hour;        // hour since midnight [0,23]
          int tm_mday;        // day of the month [1,31]
          int tm_mon;         // months since January [0,11]
          int tm_year;        // years since 1900
          int tm_wday;        // days since Sunday [0,6]
          int tm_yday;        // days since January 1 [0,365]
          int tm_isdst;       // flag for alternate daylight savings time
      };*/

    printf("%d/%d/%d %d:%d %s\n", time_info->tm_mday, time_info->tm_mon + 1, time_info->tm_year + 1900,
           time_info->tm_hour, time_info->tm_min, tzname[time_info->tm_isdst]);

    return 0;
}

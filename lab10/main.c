#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>


#define OK                  (0)
#define FAIL                (-10)
#define SYSTEM_CALL_ERROR   (-1)

#define CHILD_PID           (0)
#define MIN_ARG_COUNT       (2)


int printChildReturnCode(char * name) {
    int status;
    pid_t pid = wait(&status);
    if (pid == SYSTEM_CALL_ERROR) {
        fprintf(stderr, "Failed to wait for %s to terminate: %s\n", name, strerror(errno));
        return FAIL;
    }
    if (WIFEXITED(status)) { // NOLINT
        int retval = WEXITSTATUS(status); // NOLINT
        printf("%s exited with code %d\n", name, retval);
        return OK;
    }

    if (WIFSIGNALED(status)) { // NOLINT
        int signal = WTERMSIG(status); // NOLINT
        fprintf(stderr, "%s terminated due to the receipt of a signal %d (%s)\n", name, signal, strsignal(signal));
        return FAIL;
    }

    fprintf(stderr, "Could not determine %s's status\n", name);
    return FAIL;
}


int main(
        int argc,
        char ** argv) {
    if (argc < MIN_ARG_COUNT) {
        fprintf(stderr, "Bad number of arguments: expected at least one\n");
        return FAIL;
    }

    pid_t pid = fork();

    if (pid == SYSTEM_CALL_ERROR) {
        perror("Failed to create a subprocess");
        return FAIL;
    }
    if (pid == CHILD_PID) {
        execvp(argv[1], argv + 1);
        fprintf(stderr, "Failed to execute %s: %s\n", argv[1], strerror(errno));
        return FAIL;
    }

    int errcode = printChildReturnCode(argv[1]);
    if (errcode != OK) {
        return errcode;
    }

    return OK;
}

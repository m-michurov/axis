#include "execvpe.h"
#include <unistd.h>


extern char * const * environ;


int execvpe(
        const char * file,
        char * const argv[],
        char * const envp[]) {
    char * const * saved_env = environ;
    environ = envp;

    execvp(file, argv);

    environ = saved_env;

    return SYSTEM_CALL_ERROR;
}

#ifndef LAB11_EXECVPE_H
#define LAB11_EXECVPE_H


#define FAIL                (-10)
#define SYSTEM_CALL_ERROR   (-1)

/* Replace current process image with a new one.
 * New process image is constructed from a regular executable file.
 *
 * The 'file' argument is used to construct a pathname that identifies
 * the new process image file. If the file argument contains a slash
 * character, it is used as the pathname for this file. Otherwise, the
 * path prefix for this file is obtained by a search of the directories
 * passed in the PATH environment variable.
 *
 * PATH value used to resolve file name is obtained from 'envp'.
 * If PATH is not present in 'envp', default value PATH=/usr/bin:/bin is used.
 *
 * The 'argv' argument is an array of character pointers to null-terminated
 * strings. The last member of this array must be a null pointer. These
 * strings constitute the argument list available to the new process image.
 * The value in argv[0] should point to a filename that is associated with
 * the process being started by one of the exec functions.
 *
 * The 'envp' argument is an array of character pointers to null-terminated
 * strings. These strings constitute the environment for the new process
 * image. The envp array is terminated by a null pointer.
 */
int execvpe(
        const char * file,
        char * const argv[],
        char * const envp[]);

#endif //LAB11_EXECVPE_H

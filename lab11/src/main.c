#include <stdio.h>
#include <string.h>
#include <errno.h>


#include "execvpe/execvpe.h"


int main() {
    char * const file = "demo";
    char * const argv[] = {
            file,
            "arg1",
            "arg2",
            "arg3",
            NULL
    };
    char * const envp[] = {
            "PATH=/usr/bin/:/home/students/2019/m.michurov/axis/lab11/build/",
            "VARIABLE=VAL1",
            "ANOTHER=SOME_STRING",
            NULL
    };

    execvpe(file, argv, envp);
    fprintf(stderr, "Failed to execute %s: %s\n", file, strerror(errno));

    return FAIL;
}

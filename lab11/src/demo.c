#include <stdio.h>


#define OK  (0)


int main(
        int argc,
        char ** argv,
        char ** envp) {
    printf("Argument count: %d\n", argc);

    for (int i = 0; i < argc; ++i) {
        printf("%d: %s\n", i, argv[i]);
    }

    printf("Environment:\n");

    for (char ** current_env = envp; *current_env != NULL; ++current_env) {
        printf("%s\n", *current_env);
    }

    return OK;
}

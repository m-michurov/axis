#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>


#define OK                  (0)
#define FAIL                (-10)
#define SYSTEM_CALL_ERROR   (-1)

#define CHILD_PID           (0)
#define ARG_COUNT           (2)

#define WAIT


int waitChild() {
    int status;
    pid_t pid = wait(&status);
    if (pid == SYSTEM_CALL_ERROR) {
        perror("Failed to wait for child process to terminate");
        return FAIL;
    }
    if (!WIFEXITED(status)) { // NOLINT
        fprintf(stderr, "Child process did not terminate normally\n");
        return FAIL;
    }

    return OK;
}


int main(
        int argc,
        char ** argv) {
    if (argc != ARG_COUNT) {
        fprintf(stderr, "Bad number of arguments: expected one\n");
        return FAIL;
    }

    pid_t pid = fork();

    if (pid == SYSTEM_CALL_ERROR) {
        perror("Failed to create a subprocess");
        return FAIL;
    }
    if (pid == CHILD_PID) {
        execlp("cat", "cat", argv[1], NULL);
        perror("Failed to execute cat");
        return FAIL;
    }
#ifdef WAIT
    int errcode = waitChild();
    if (errcode != OK) {
        return errcode;
    }
#endif
    printf("Hello from parent process!\n");
    return OK;
}

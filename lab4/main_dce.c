#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>


#define BLOCK_SIZE LINE_MAX


typedef struct st_ListNode Node;

Node * newStringNode(char const * string);

void destroyList(Node ** node);

void printListReversed(Node * list);

int pushFront(Node ** list, char const * string);


int main() {
    printf("%ld\n", BLOCK_SIZE);
    char * buff = (char *) malloc(BLOCK_SIZE * sizeof(char));

    Node * list = NULL;
    char * result;
    int errcode;

    while (1) {
        result = fgets(buff, BLOCK_SIZE, stdin);

        if (result == NULL || *buff == '.') {
	    break;
	}
        errcode = pushFront(&list, buff);

        if (errcode != 0) {
            printf("failed to append new node");
            destroyList(&list);
            return -1;
        }
    }

    printListReversed(list);
    destroyList(&list);
    free((void *) buff);

    return 0;
}


struct st_ListNode {
    char * string;
    Node * next;
};


Node * newStringNode(char const * string) {
    Node * new_node = (Node *) malloc(sizeof(Node));

    if (new_node == NULL) {
        return NULL;
    }

    new_node->next = NULL;
    new_node->string = (char *) malloc((strlen(string) + 1) * sizeof(char));

    if (new_node->string == NULL) {
        free((void *) new_node);
        return NULL;
    }

    // printf("Allocated %d bytes for list node containing \"%s\"\n", 
    //	(strlen(string) + 1) * sizeof(char), string); 

    strcpy(new_node->string, string);

    return new_node;
}


void destroyList(Node ** node) {
    if (node == NULL || *node == NULL) {
        return;
    }
    destroyList(&(*node)->next);
    free((void *) (*node)->string);
    free((void *) *node);
    *node = NULL;
}


int pushFront(Node ** list, char const * string) {
    Node * new_node = newStringNode(string);
    if (new_node == NULL) {
        return -1;
    }

    new_node->next = *list;
    *list = new_node;

    return 0;
}


void printListReversed(Node * list) {
    if (list == NULL) {
	return;
    }
    printListReversed(list->next);
    printf("%s", list->string);
}

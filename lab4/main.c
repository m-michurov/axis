#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#ifndef BLOCK_SIZE
#define BLOCK_SIZE BUFSIZ
#endif


typedef struct st_ListNode Node;

typedef struct st_LinkedList LinkedList;
// Since list object is represented by two pointers (head and tail) instead of one (head),
// it's more convenient to store both in one struct
struct st_LinkedList {
    Node * head_ptr;
    Node * tail_ptr;
};

// Functions that work with list now take pointer to LinkedList instead of pointer to head

// Free memory used by list
void destroyList(LinkedList * list);

// Print list contents to stream, inserting delimiter between adjacent strings
void fprintList(
        FILE * stream,
        LinkedList * list,
        char const * delimiter); // Added delimiter parameter for more flexibility and to simplify possible code reuse

// Append a new string to list
int pushBack(
        LinkedList * list,
        char const * string,
        int length);


int main() {
    char buff[BLOCK_SIZE] = { 0 };

    LinkedList list = { NULL, NULL }; // Initialize both head and tail pointers to null, i.e. create an empty list
    char * result;
    int errcode;
    int currentStringLength;

    while (1) {
        result = fgets(buff, BLOCK_SIZE, stdin);
        if (result == NULL || *buff == '.') {
            break;
        }

        currentStringLength = strlen(buff);
        // printf("Current: %d\n", currentStringLength);

        if (currentStringLength == BLOCK_SIZE - 1 && buff[BLOCK_SIZE - 2] != '\n') {
            fprintf(stderr, "Line is too long and cannot be handled correctly. Maximum allowed length including '\\n' is %d\n", BLOCK_SIZE - 1);
            destroyList(&list);
            return -1;
        }

        if (buff[currentStringLength - 1] == '\n') {
            buff[currentStringLength - 1] = 0;
        }

        errcode = pushBack(&list, buff, currentStringLength - 1);
        if (errcode != 0) {
            perror("Failed to append new node");
            destroyList(&list);
            return -1;
        }
    }

    fprintList(stdout, &list, "\n");
    printf("\n");
    destroyList(&list);

    return 0;
}


struct st_ListNode {
    char * string_ptr;
    Node * next_ptr;
};


Node * newStringNode(char const * string, int length) {
    if (length < 0) {
        return NULL;
    }

    Node * new_node = (Node *) malloc(sizeof(Node));
    if (new_node == NULL) {
        return NULL;
    }

    new_node->next_ptr = NULL;
    new_node->string_ptr = (char *) malloc((length + 1) * sizeof(char));

    if (new_node->string_ptr == NULL) {
        free((void *) new_node);
        return NULL;
    }

    strcpy(new_node->string_ptr, string);

    return new_node;
}


void destroyList(LinkedList * list) {
    // If list pointer is NULL
    // or list pointer is not NULL, but list's head pointer is NULL
    // then there's nothing to destroy
    if (list == NULL || list->head_ptr == NULL) {
        return;
    }

    // For each node (until there are no more nodes)
    //     1. Save pointer to next node
    //     2. Free memory used to store current node's string
    //     3. Free memory used to store current node
    //     4. Move on to the next node
    for (
            Node * current = list->head_ptr, * next; // Init pointer to current node, define pointer to next node
            current != NULL; // Continue until current node pointer is NULL, i.e. all nodes have been freed
            current = next // (4)
            ) {
        next = current->next_ptr; // (1)
        free((void *) current->string_ptr); // (2)
        free((void *) current); // (3)
    }

    // Assign NULL both to head and tail pointers, making this list a valid empty list
    list->head_ptr = NULL;
    list->tail_ptr = NULL;
}


int pushBack(
        LinkedList * list,
        char const * string,
        int length) {
    // Attempting to push another string to a non-existent list is an error
    if (list == NULL) {
        return -1;
    }

    Node * new_node_ptr = newStringNode(string, length);

    if (new_node_ptr == NULL) {
        return -1;
    }

    // If list is empty, then new node becomes it's head
    if (list->head_ptr == NULL) {
        list->head_ptr = new_node_ptr;
    }
        // Else the new node is appended to the last node, which is pointed by tail_ptr
    else {
        list->tail_ptr->next_ptr = new_node_ptr;
    }

    // Either way, the new node becomes the tail
    list->tail_ptr = new_node_ptr;

    return 0;
}


void fprintList(
        FILE * stream,
        LinkedList * list,
        char const * delimiter) {
    // If there's no list or list is empty
    // then there's nothing to print
    if (list == NULL || list->head_ptr == NULL) {
        return;
    }

    for (
            Node * current = list->head_ptr; // Current node pointer is initialized with list's head
            current->next_ptr != NULL; // Continue until next node pointer is NULL, i.e. only one node left
            current = current->next_ptr // Move on to the next node
            ) {
        // Print current node's string and the delimiter to stream
        fprintf(stream, "%s%s", current->string_ptr, delimiter);
    }

    fprintf(stream, "%s", list->tail_ptr->string_ptr); // There should be no delimiter after the last string
}

#ifndef LAB5_ERROR_H
#define LAB5_ERROR_H


#define FAIL                    -10
#define OK                      0
#define SYSTEM_CALL_ERROR       -1
#define NULL_POINTER_ERROR      -2
#define BAD_ARGUMENT_RANGE      -3
#define INPUT_IS_NON_INTEGER    -4
#define BAD_INPUT_FORMAT        -5
#define BAD_INPUT_RANGE         -6


// Print error message and possibly error cause (if any) to stderr
// If errcode is one of defined above, a more detailed message describing error cause is printed
void reportError(
        char const * description,
        int errcode);

#endif //LAB5_ERROR_H

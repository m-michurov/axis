cmake_minimum_required(VERSION 3.0)
project(lab5 C)

add_compile_options(-std=c99)

add_executable(
        lab5
        src/main.c
        src/error_reporter/error.c
        src/utility/utility.c
        src/utility/line_data_table.c
)
#include "error.h"


#include <stdio.h>


void reportError(
        char const * description,
        int errcode) {
    if (description == NULL && errcode == OK) {
        return;
    }
    else if (description != NULL && errcode == OK) {
        fprintf(stderr, "%s.\n", description);
    }
    else {
        char const * errorInfo;

        switch (errcode) {
            case NULL_POINTER_ERROR:
                errorInfo = "Unexpected null pointer";
                break;
            case BAD_ARGUMENT_RANGE:
                errorInfo = "Argument out of allowed range";
                break;
            case BAD_INPUT_RANGE:
                errorInfo = "Input out of allowed range";
                break;
            case BAD_INPUT_FORMAT:
                errorInfo = "Bad input format";
                break;
            case INPUT_IS_NON_INTEGER:
                errorInfo = "Input is expected to be a single integer but is not";
                break;
            case SYSTEM_CALL_ERROR:
                errorInfo = "System call error";
                break;
            default:
                errorInfo = "Error";
        }

        fprintf(stderr, "%s. %s%s\n", errorInfo, description == NULL ? "" : description, description == NULL ? "" : ".");
    }
}

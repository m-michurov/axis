#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>


#include "utility/line_data_table.h"
#include "error_reporter/error.h"
#include "utility/utility.h"


#define BUFFER_SIZE BUFSIZ


int prepareData(
        int argc,
        char ** argv,
        char const ** pErrorDescription,
        int * pFileDesc,
        LineDataTable * pDataTable);


int runUserInteraction(
        char const ** pErrorDescription,
        int fileDesc,
        LineDataTable * pDataTable);


int cleanup(
        int fileDesc,
        LineDataTable * pLineData);


int main(
        int argc,
        char ** argv) {
    char const * errorDescription = NULL;
    int errcode;

    int fileDesc;

    LineDataTable lineDataTable = { NULL, 0, 0 };

    errcode = prepareData(argc, argv, &errorDescription, &fileDesc, &lineDataTable);
    if (errcode != OK) {
        reportError(errorDescription, errcode);
        return FAIL;
    }

    errcode = runUserInteraction(&errorDescription, fileDesc, &lineDataTable);
    reportError(errorDescription, errcode);

    return cleanup(fileDesc, &lineDataTable);
}


int prepareData(
        int argc,
        char ** argv,
        char const ** pErrorDescription,
        int * pFileDesc,
        LineDataTable * pDataTable) {
    if (pErrorDescription == NULL || argv == NULL || pFileDesc == NULL || pDataTable == NULL) {
        fprintf(stderr, "Failed to prepare necessary data: invalid program state unrelated to user input.\n");
        return NULL_POINTER_ERROR;
    }

    if (argc < 2) {
        *pErrorDescription = "No input file specified";
        return FAIL;
    }

    char * inputFilePath = argv[1];

    int fileDesc = open(inputFilePath, O_RDONLY);
    if (fileDesc == SYSTEM_CALL_ERROR) {
        fprintf(stderr, "Failed to open \"%s\": %s\n", inputFilePath, strerror(errno));
        return FAIL;
    }

    int errcode = fillOffsetTable(pErrorDescription, fileDesc, pDataTable);
    if (errcode != OK) {
        int auxErrcode = close(fileDesc);
        if (auxErrcode == SYSTEM_CALL_ERROR) {
            perror("Failed to close input file after data preparation error");
        }
        return errcode;
    }

    *pFileDesc = fileDesc;

    return errcode;
}


int runUserInteraction(
        char const ** pErrorDescription,
        int fileDesc,
        LineDataTable * pDataTable) {
    if (pErrorDescription == NULL || pDataTable == NULL) {
        fprintf(stderr, "Failed to run user interaction: invalid program state unrelated to user input.\n");
        return NULL_POINTER_ERROR;
    }

    int errcode;
    char buffer[BUFFER_SIZE] = { 0 };
    int lineIndex;
    int length;
    int offset;
    int maxAllowedIndex = getLastIndex(pDataTable);

    printf("Enter line number from allowed range [1, %d] or 0 to exit.\n", maxAllowedIndex);
    while (1) {
        printf("Enter line number:\n");
        errcode = getLineNumber(&lineIndex, pErrorDescription, buffer, BUFFER_SIZE);
        if (errcode != OK && errcode != BAD_INPUT_RANGE) {
            return errcode;
        }

        if (lineIndex < 0 || lineIndex > maxAllowedIndex || errcode == BAD_INPUT_RANGE) {
            fprintf(stderr, "Allowed range is [1, %d].\n", maxAllowedIndex);
            *pErrorDescription = "Line number out of allowed range";
            return BAD_INPUT_RANGE;
        }
        if (lineIndex == 0) {
            break;
        }

        length = getLength(pDataTable, lineIndex);
        offset = getOffset(pDataTable, lineIndex);

        errcode = printLineFromFile(fileDesc, length, offset, buffer, BUFFER_SIZE);
        if (errcode != OK) {
            *pErrorDescription = "Failed to reprint line";
            return errcode;
        }
    }

    return OK;
}


int cleanup(
        int fileDesc,
        LineDataTable * pLineData) {
    clearOffsetTable(pLineData);
    int errcode = close(fileDesc);
    if (errcode == SYSTEM_CALL_ERROR) {
        perror("Failed to close input file");
    }
    return errcode;
}

#ifndef LAB5_UTILITY_H
#define LAB5_UTILITY_H


#include "line_data_table.h"
#include "../error_reporter/error.h"


int fillOffsetTable(const char **pErrorDescription, int fileDesc, LineDataTable *dataTable);


int printLineFromFile(
        int fileDesc,
        int length,
        int offset,
        char * buffer,
        int bufferSize);


int getLineNumber(
        int * result,
        char const ** pErrorDescription,
        char * buffer,
        int bufferSize);


#endif //LAB5_UTILITY_H

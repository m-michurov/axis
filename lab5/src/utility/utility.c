#include "utility.h"


#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>


int getNextLineLength(
        int fileDesc,
        int * isLast) {
    if (isLast == NULL) {
        return NULL_POINTER_ERROR;
    }

    int currentLineLength = 0;
    int charsRead;
    char currentChar;

    do {
        charsRead = read(fileDesc, &currentChar, 1);
        if (charsRead == SYSTEM_CALL_ERROR) {
            perror("Failed to read next byte to determine line length");
            return SYSTEM_CALL_ERROR;
        }
        currentLineLength += charsRead;
    } while (charsRead == 1 && currentChar != '\n');

    *isLast = charsRead == 0;

    return currentLineLength;
}


int fillOffsetTable(
        const char ** pErrorDescription,
        int fileDesc,
        LineDataTable * dataTable) {
    if (pErrorDescription == NULL || dataTable == NULL) {
        fprintf(stderr, "Failed to fill offset table: invalid program state unrelated to user input.\n");
        return NULL_POINTER_ERROR;
    }
    int currentLineLength = 0;
    int lineIndex = 1;
    int currentOffset = 0;
    int isLast = 0;
    int errcode;

    errcode = setOffset(dataTable, 1, 0);
    if (errcode != OK) {
        *pErrorDescription = "Failed to fill offset table: failed to set line offset due to memory allocation error";
        return errcode;
    }

    while (1) {
        currentLineLength = getNextLineLength(fileDesc, &isLast);
        if (currentLineLength < 0) {
            *pErrorDescription = "Failed to fill offset table: failed to get length of line in input file";
            return currentLineLength;
        }
        if (isLast) {
            break;
        }

        currentOffset = lseek(fileDesc, 0, SEEK_CUR);
        if (currentOffset == SYSTEM_CALL_ERROR) {
            perror("Failed to fill offset table: unable to get current position in input file");
            return SYSTEM_CALL_ERROR;
        }
        errcode = setLength(dataTable, lineIndex, currentLineLength - 1);
        if (errcode != OK) {
            *pErrorDescription = "Failed to fill offset table: failed to set line length due to memory allocation error";
            return errcode;
        }
        errcode = setOffset(dataTable, lineIndex + 1, currentOffset);
        if (errcode != OK) {
            *pErrorDescription = "Failed to fill offset table: failed to set line offset due to memory allocation error";
            return errcode;
        }
        lineIndex += 1;
    }

    errcode = setLength(dataTable, lineIndex, currentLineLength);
    if (errcode != OK) {
        *pErrorDescription = "Failed to fill offset table: failed to set line length";
    }

    return errcode;
}


int printLineFromFile(
        int fileDesc,
        int length,
        int offset,
        char * buffer,
        int bufferSize) {
    if (bufferSize < 1) {
        return BAD_ARGUMENT_RANGE;
    }

    int maxReadSize = bufferSize - 1;
    int charsRead;
    int errcode;

    errcode = lseek(fileDesc, offset, SEEK_SET);
    if (errcode == SYSTEM_CALL_ERROR) {
        perror("Failed to move file pointer to the start of requested line");
        return SYSTEM_CALL_ERROR;
    }

    int rest = length;

    while (rest > 0) {
        charsRead = read(fileDesc, buffer, rest > maxReadSize ? maxReadSize : rest);
        if (charsRead == SYSTEM_CALL_ERROR || charsRead == 0) {
            perror("Failed to read requested line");
            return SYSTEM_CALL_ERROR;
        }
        buffer[charsRead] = 0;

        printf("%s", buffer);
        rest -= charsRead;
    }

    printf("\n");

    return OK;
}


int parseInt(
        char * buffer,
        unsigned int length,
        int * resultInt) {
    if (resultInt == NULL) {
        return NULL_POINTER_ERROR;
    }

    char * nextCharPtr;
    long result = strtol(buffer, &nextCharPtr, 10);

    if (nextCharPtr == buffer || errno == EINVAL) {
        return INPUT_IS_NON_INTEGER;
    }

    for (char * it = nextCharPtr; it != buffer + length; ++it) {
        if (!isspace(*it) && *it != '\n') {
            return INPUT_IS_NON_INTEGER;
        }
    }

    if (errno == ERANGE || result > INT_MAX || result < INT_MIN) {
        return BAD_INPUT_RANGE;
    }

    *resultInt = (int) result;

    return OK;
}


int getLineNumber(
        int * result,
        char const ** pErrorDescription,
        char * buffer,
        int bufferSize) {
    if (result == NULL || pErrorDescription == NULL || buffer == NULL) {
        fprintf(stderr, "Failed to get line number: invalid program state unrelated to user input.\n");
        return NULL_POINTER_ERROR;
    }
    if (bufferSize < 4) {
        fprintf(stderr, "Failed to get line number: invalid program state unrelated to user input.\n");
        return BAD_ARGUMENT_RANGE;
    }

    int errcode;
    int lineNumber;

    char * retPtr = fgets(buffer, bufferSize, stdin);
    if (retPtr != buffer) {
        *pErrorDescription = "Failed to get line number: unable to read input from standard input stream";
        return FAIL;
    }

    unsigned int length = strlen(buffer);
    if (length == bufferSize - 1 && buffer[length - 1] != '\n') {
        fprintf(stderr, "Please limit your input to %d characters\n", bufferSize - 2);
        *pErrorDescription = "Input is too long";
        return BAD_INPUT_FORMAT;
    }

    errcode = parseInt(buffer, length, &lineNumber);
    if (errcode != OK) {
        *pErrorDescription = "Failed to get line number: bad input format";
        return errcode;
    }

    *result = lineNumber;
    return OK;
}

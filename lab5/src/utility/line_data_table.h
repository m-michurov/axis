#ifndef LAB5_LINE_DATA_H
#define LAB5_LINE_DATA_H


#include <errno.h>
#include <unistd.h>
#include <fcntl.h>


typedef struct st_LineData LineData;
typedef struct st_LineDataTable LineDataTable;

struct st_LineDataTable {
    LineData * m_data;
    int m_current_size;
    int m_max_size;
};

// Free memory and reset size to 0
// Object pointed by dataTable can be used after clearOffsetTable as an empty table
void clearOffsetTable(LineDataTable * dataTable);

int setLength(
        LineDataTable * dataTable,
        int lineIndex,
        int length);

int getLength(
        LineDataTable * dataTable,
        int lineIndex);

int setOffset(
        LineDataTable * dataTable,
        int lineIndex,
        off_t length);

off_t getOffset(
        LineDataTable * dataTable,
        int lineIndex);

// Get maximum index stored by table pointed by dataTable
int getLastIndex(LineDataTable * dataTable);

#endif //LAB5_LINE_DATA_H

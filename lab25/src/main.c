#include <stdio.h>
#include <unistd.h>
#include <ctype.h>

#include "util/util.h"
#include "err.h"


#define CH0_READ_FD         (1)
#define CH0_WRITE_FD        (0)
#define CH1_READ_FD         (1)
#define CH1_WRITE_FD        (0)

#define BUFFER_SIZE         (BUFSIZ)

#define PIPE_FDS_COUNT      (2)
#define CHILD_PID           (0)
#define TOTAL_CHILDREN      (2)

#define forever             for (;;)


int write_message(int fd) {
    unsigned char const message[] = "PooRlYfOrMattEDtExT";
    int err;

    err = write(fd, message, sizeof(message));
    if (err == SYSTEM_CALL_ERROR) {
        perror("Failed to write a message");
        return SYSTEM_CALL_ERROR;
    }

    return OK;
}


int read_and_process_message(int fd) {
    unsigned char buffer[BUFFER_SIZE] = { 0 };
    int chars_read;

    forever {
        chars_read = read(fd, buffer, BUFFER_SIZE - 1);
        if (chars_read == SYSTEM_CALL_ERROR) {
            perror("Failed to read a message");
            return SYSTEM_CALL_ERROR;
        }
        if (chars_read == 0) {
            break;
        }

        buffer[chars_read] = 0;
        for (int i = 0; i < chars_read; i += 1) {
            if (islower(buffer[i])) {
                buffer[i] = toupper(buffer[i]);
            }
        }
        printf("%s", buffer);
    }

    printf("\n");

    return OK;
}


int do_writer_work(int fds[PIPE_FDS_COUNT]) {
    int err = close_desc(fds[CH0_READ_FD]);
    if (err == SYSTEM_CALL_ERROR) {
        close_desc(fds[CH0_WRITE_FD]);
        return SYSTEM_CALL_ERROR;
    }

    err = write_message(fds[CH0_WRITE_FD]);

    close_desc(fds[CH0_WRITE_FD]);
    return err;
}


int do_reader_work(int fds[PIPE_FDS_COUNT]) {
    int err = close_desc(fds[CH1_WRITE_FD]);
    if (err == SYSTEM_CALL_ERROR) {
        close_desc(fds[CH1_READ_FD]);
        return SYSTEM_CALL_ERROR;
    }

    err = read_and_process_message(fds[CH1_READ_FD]);

    close_desc(fds[CH1_READ_FD]);
    return err;
}


int main() {
    int fds[PIPE_FDS_COUNT];
    int err;

    err = pipe(fds);
    if (err == SYSTEM_CALL_ERROR) {
        perror("Failed to create a pipe");
        return SYSTEM_CALL_ERROR;
    }

    pid_t pid = fork();
    if (pid == SYSTEM_CALL_ERROR) {
        perror("Failed to spawn first subprocess");
        return SYSTEM_CALL_ERROR;
    }
    if (pid == CHILD_PID) {
        return do_writer_work(fds);
    }

    pid = fork();
    if (pid == SYSTEM_CALL_ERROR) {
        perror("Failed to spawn second subprocess");
        close_all(fds, PIPE_FDS_COUNT);
        return SYSTEM_CALL_ERROR;
    }
    if (pid == CHILD_PID) {
        return do_reader_work(fds);
    }

    close_all(fds, PIPE_FDS_COUNT);
    wait_multiple(TOTAL_CHILDREN);
    return OK;
}

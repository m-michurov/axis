#ifndef LAB25_UTIL_H
#define LAB25_UTIL_H

void wait_multiple(int count);

int close_desc(int fd);

void close_all(
        int * fds,
        int count);


#endif //LAB25_UTIL_H

#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <wait.h>

#include "../err.h"
#include "util.h"


void wait_child() {
    int status;
    pid_t pid = wait(&status);

    if (pid == SYSTEM_CALL_ERROR) {
        perror("Failed to wait for child process to terminate");
        return;
    }
    if (!WIFEXITED(status)) { // NOLINT
        fprintf(stderr, "Child process with pid %d did not terminate normally\n", pid);
        return;
    }
}


void wait_multiple(int count) {
    for (int i = 0; i < count; i += 1) {
        wait_child();
    }
}


int close_desc(int fd) {
    int err = close(fd);
    if (err == SYSTEM_CALL_ERROR) {
        fprintf(stderr, "Failed to close fd %d: %s\n", fd, strerror(errno));
        return SYSTEM_CALL_ERROR;
    }

    return OK;
}


void close_all(
        int * fds,
        int count) {
    for (int i = 0; i < count; i += 1) {
        close_desc(fds[i]);
    }
}
